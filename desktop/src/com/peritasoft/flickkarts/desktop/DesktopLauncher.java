package com.peritasoft.flickkarts.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.github.czyzby.websocket.CommonWebSockets;
import com.peritasoft.flickkarts.FlickKarts;

public class DesktopLauncher {
    public static void main(String[] arg) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setWindowedMode(1280, 720);
        CommonWebSockets.initiate();
        final FlickKarts game = new FlickKarts();
        if (arg.length > 1 && arg[0].equals("--player-name")) {
            game.setPlayerName(arg[1]);
        }
        new Lwjgl3Application(game, config);
    }
}
