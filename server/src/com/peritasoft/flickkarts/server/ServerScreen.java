package com.peritasoft.flickkarts.server;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.utils.Array;
import com.peritasoft.flickkarts.items.Item;
import com.peritasoft.flickkarts.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ServerScreen extends ScreenAdapter implements Server.Listener {
    private static final Logger log = LoggerFactory.getLogger(ServerScreen.class);
    private final Server server;
    private final Array<ServerRace> races;
    private final Map<String, ServerLobby> lobbies;

    public ServerScreen() {
        races = new Array<ServerRace>(false, 16);
        server = new Server(this);
        lobbies = new HashMap<String, ServerLobby>();
    }

    @Override
    public void show() {
        server.start();
    }

    @Override
    public void render(float delta) {
        for (int i = races.size - 1; i >= 0; i--) {
            ServerRace race = races.get(i);
            race.update(delta);
            if (race.isFinished()) {
                races.removeIndex(i);
                ServerLobby lobby = lobbies.get(race.getRaceCode());
                if (lobby != null) lobby.endGame();
                race.dispose();
            }
        }
    }

    public String generateRaceCode() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 6;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    @Override
    public void onLogin(ServerPlayer player, PlayerLogin login) {
        player.setName(login.getPlayerName());
        String raceCode = login.getRaceCode();
        if (raceCode == null) {
            do {
                raceCode = generateRaceCode();
            } while (lobbies.containsKey(raceCode));
            final ServerLobby serverLobby = new ServerLobby(raceCode);
            lobbies.put(raceCode, serverLobby);
            log.debug("Created {}", serverLobby);
        }
        final ServerLobby lobby = getLobbyToJoin(player, raceCode);
        if (lobby != null) {
            lobby.add(player);
        }
    }

    private ServerLobby getLobbyToJoin(final ServerPlayer player, final String raceCode) {
        if (lobbies.containsKey(raceCode)) {
            ServerLobby lobby = lobbies.get(raceCode);
            if (lobby.acceptPlayers()) {
                log.debug("{} joined {}", player, lobby);
                return lobby;
            } else {
                player.send(new LoginError(LoginError.ErrorCode.LOBBY_NOT_AVAILABLE));
                log.debug("Race: " + raceCode + " does not accept players");
            }
        } else {
            player.send(new LoginError(LoginError.ErrorCode.LOBBY_NOT_EXIST));
            log.debug("Race: " + raceCode + " does not exist");
        }
        return null;
    }

    @Override
    public void onFlick(ServerPlayer player, Flick flick) {
        final ServerRace race = player.getRace();
        if (race != null) {
            race.flick(player.getId(), flick.getImpulse());
        }
    }

    @Override
    public void onLogout(ServerPlayer player) {
        String raceCode = player.getRaceCode();
        final ServerLobby lobby = lobbies.get(raceCode);
        if (lobby != null) {
            lobby.remove(player);
            log.debug("{} left {}", player, lobby);
            if (lobby.isEmpty()) {
                lobbies.remove(raceCode);
                log.debug("Destroyed {}", lobby);
            }
        }
        final ServerRace race = player.getRace();
        if (race != null) {
            race.logoutPlayer(player);
        }
    }

    @Override
    public void onStartGame(ServerPlayer player, StartGame packet) {
        ServerLobby lobby = lobbies.get(player.getRaceCode());
        try {
            races.add(lobby.startGame());
        } catch (RuntimeException ex) {
            log.error("Could not start game for " + lobby, ex);
        }
    }

    @Override
    public void onRematch(ServerPlayer player, Rematch packet) {
        final ServerLobby lobby = getLobbyToJoin(player, player.getRaceCode());
        if (lobby != null) {
            lobby.sendLoggedIn(player);
        }
    }

    @Override
    public void onChangeRaceLaps(ServerPlayer player, ChangeRaceLaps packet) {
        ServerLobby lobby = lobbies.get(player.getRaceCode());
        if (lobby != null) {
            lobby.setLaps(packet.getLaps());
        }
    }

    @Override
    public void onChangeTrack(ServerPlayer player, ChangeTrack packet) {
        ServerLobby lobby = lobbies.get(player.getRaceCode());
        if (lobby != null) {
            lobby.setTrack(packet.getTrack());
        }
    }

    @Override
    public void onUseItem(ServerPlayer player, UseItem packet) {
        final ServerRace race = player.getRace();
        if (race != null) {
            race.kartUseItem(player.getId());
        }
    }

    @Override
    public void dispose() {
        try {
            server.stop(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = races.size - 1; i >= 0; i--) {
            ServerRace race = races.get(i);
            race.dispose();
        }
    }
}