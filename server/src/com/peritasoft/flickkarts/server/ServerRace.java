package com.peritasoft.flickkarts.server;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.flickkarts.Kart;
import com.peritasoft.flickkarts.KartColor;
import com.peritasoft.flickkarts.Race;
import com.peritasoft.flickkarts.items.Burst;
import com.peritasoft.flickkarts.items.Item;
import com.peritasoft.flickkarts.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ServerRace implements Disposable, Race.Listener {
    private static final Logger log = LoggerFactory.getLogger(ServerRace.class);

    private final Race race;
    private final Array<ServerPlayer> players;
    private final String raceCode;
    private boolean isKartFlicking;
    private boolean raceFinishedSent;

    public ServerRace(final Race.Parameters parameters, final Array<ServerPlayer> players, String raceCode) {
        players.shuffle();
        parameters.initialPlayer = players.get(0).getId();
        race = new Race(parameters, players, new ServerMapLoader(), this, null);
        this.players = players;
        isKartFlicking = false;
        this.raceCode = raceCode;
        raceFinishedSent = false;
        startGame(parameters);
    }

    public void update(float delta) {
        race.update();
        if (isKartFlicking && race.flickFinished()) {
            nextTurn();
        }
        if (race.isFinished()) {
            sendRaceFinished();
        }
    }

    private void sendRaceFinished() {
        if (raceFinishedSent) return;
        ArrayList<String> players = new ArrayList<String>(race.getClassification().size());
        for (Kart kart : race.getClassification()) {
            players.add(kart.getPlayerName());
        }
        sendToAll(new RaceFinished(players));
        raceFinishedSent = true;
    }

    private void nextTurn() {
        ArrayList<Kart> classified = new ArrayList<Kart>();
        race.nextTurn(classified);
        log.debug("Race {}: Classified: {}", raceCode, classified);
        for (Kart kart : classified) {
            classifyPlayer(kart.getId());
        }
        sendToAll(new NextTurn(race.getCurrentId(), getKartsState(), race.getCurrentRound()));
        isKartFlicking = false;
    }

    private void startGame(final Race.Parameters raceParameters) {
        sendToAll(new GameStarted(raceParameters, buildPlayerMap(), buildPlayersColorMap(), getKartsState()));
    }

    private Map<String, String> buildPlayerMap() {
        HashMap<String, String> playerMap = new HashMap<String, String>();
        for (int i = 0; i < players.size; i++) {
            ServerPlayer player = players.get(i);
            playerMap.put(String.valueOf(player.getId()), player.getName());
        }
        return playerMap;
    }

    private Map<String, KartColor> buildPlayersColorMap() {
        HashMap<String, KartColor> playersColorMap = new HashMap<String, KartColor>();
        for (int i = 0; i < players.size; i++) {
            ServerPlayer player = players.get(i);
            playersColorMap.put(String.valueOf(player.getId()), player.getColor());
        }
        return playersColorMap;
    }

    private ArrayList<KartState> getKartsState() {
        ArrayList<KartState> kartsState = new ArrayList<KartState>();
        for (Kart kart : race.getKarts()) {
            kartsState.add(new KartState(kart.getId(), kart.getPosition(), kart.getAngle(), kart.getCurrentSector(), kart.getLap()));
        }
        return kartsState;
    }

    public void flick(int playerId, Vector2 impulse) {
        if (race.isCurrentKart(playerId) && !isKartFlicking) {
            isKartFlicking = true;
            race.flick(playerId, impulse);
            sendToAllExcept(playerId, new Flick(playerId, impulse));
        }
    }

    private void sendToAll(Object msg) {
        for (int i = 0; i < players.size; i++) {
            ServerPlayer player = players.get(i);
            player.send(msg);
        }
    }

    private void sendToAllExcept(int playerId, Object msg) {
        for (int i = 0; i < players.size; i++) {
            ServerPlayer player = players.get(i);
            if (player.getId() != playerId) player.send(msg);
        }
    }

    private void sendTo(int playerId, Object msg) {
        for (int i = 0; i < players.size; i++) {
            ServerPlayer player = players.get(i);
            if (player.getId() == playerId) {
                player.send(msg);
                return;
            }
        }
    }

    public void logoutPlayer(ServerPlayer player) {
        players.removeValue(player, true);
        sendToAll(new PlayerLeftRace(player.getId()));
        boolean activeRemoved = race.removeKart(player.getId());
        if (race.isFinished()) {
            sendRaceFinished();
        } else if (activeRemoved) {
            log.info("Active Kart Logout");
            nextTurn();
        }
    }

    public void classifyPlayer(int id) {
        sendToAll(new KartClassified(id));
    }

    public boolean isFinished() {
        return race.isFinished();
    }

    public String getRaceCode() {
        return raceCode;
    }

    @Override
    public void dispose() {
        race.dispose();
    }

    @Override
    public void generateRandomItem(Kart kart) {
        Item item = new Burst();
        kart.pickItem(item);
        sendTo(kart.getId(), new PickItem(kart.getId(), item));
    }

    public void kartUseItem(int id) {
        race.kartUseItem(id);
    }
}