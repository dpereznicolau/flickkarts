package com.peritasoft.flickkarts.server;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.SerializationException;
import com.peritasoft.flickkarts.messages.*;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

public class Server extends WebSocketServer {
    private static final Logger log = LoggerFactory.getLogger(Server.class);

    private final Json jsonSerializer;
    private final Listener listener;

    public Server(final Listener listener) {
        super(new InetSocketAddress("0.0.0.0", 8080));
        setReuseAddr(true);
        jsonSerializer = new Json();
        this.listener = listener;
    }

    private static int getPlayerId(WebSocket conn) {
        return conn.getRemoteSocketAddress().hashCode();
    }

    private static ServerPlayer getPlayer(WebSocket conn) {
        return conn.getAttachment();
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        final ServerPlayer player = new ServerPlayer(getPlayerId(conn), conn, jsonSerializer);
        conn.setAttachment(player);
        log.debug("{} connected from {}", player, conn.getRemoteSocketAddress());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        final ServerPlayer player = getPlayer(conn);
        log.debug("{} disconnected from {}: {}", player, conn.getRemoteSocketAddress(), reason);
        listener.onLogout(player);
        conn.setAttachment(null);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        onMessage(conn, deserialize(message));
    }

    private void onMessage(final WebSocket conn, final Object packet) {
        final ServerPlayer player = getPlayer(conn);
        if (player == null) {
            log.error("Connection from {} has no player", conn.getRemoteSocketAddress());
            return;
        }
        log.debug("Received from {}: {}", player, packet);
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (packet instanceof PlayerLogin) {
                    listener.onLogin(player, (PlayerLogin) packet);
                } else if (packet instanceof Flick) {
                    listener.onFlick(player, (Flick) packet);
                } else if (packet instanceof UseItem) {
                  listener.onUseItem(player, (UseItem) packet);
                } else if (packet instanceof StartGame) {
                    listener.onStartGame(player, (StartGame) packet);
                } else if (packet instanceof Rematch) {
                    listener.onRematch(player, (Rematch) packet);
                } else if (packet instanceof ChangeRaceLaps) {
                    listener.onChangeRaceLaps(player, (ChangeRaceLaps) packet);
                } else if (packet instanceof ChangeTrack) {
                    listener.onChangeTrack(player, (ChangeTrack) packet);
                }
            }
        });
    }

    private Object deserialize(String message) {
        try {
            return jsonSerializer.fromJson(null, message);
        } catch (SerializationException ex) {
            log.error("Failed to parse message: {}: {}", message, ex.getMessage());
            throw ex;
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        log.error(ex.getMessage());
    }

    @Override
    public void onStart() {
        log.info("Listening on port {}", getPort());
    }

    public interface Listener {
        void onLogin(ServerPlayer player, PlayerLogin login);

        void onFlick(ServerPlayer player, Flick flick);

        void onLogout(ServerPlayer player);

        void onStartGame(ServerPlayer player, StartGame packet);

        void onRematch(ServerPlayer player, Rematch packet);

        void onChangeRaceLaps(ServerPlayer player, ChangeRaceLaps packet);

        void onChangeTrack(ServerPlayer player, ChangeTrack packet);

        void onUseItem(ServerPlayer player, UseItem packet);
    }
}
