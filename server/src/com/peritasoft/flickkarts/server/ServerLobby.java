package com.peritasoft.flickkarts.server;

import com.badlogic.gdx.utils.Array;
import com.peritasoft.flickkarts.KartColor;
import com.peritasoft.flickkarts.Player;
import com.peritasoft.flickkarts.Race;
import com.peritasoft.flickkarts.Track;
import com.peritasoft.flickkarts.messages.LoggedIn;
import com.peritasoft.flickkarts.messages.PlayerJoined;
import com.peritasoft.flickkarts.messages.PlayerLeftLobby;
import com.peritasoft.flickkarts.messages.RaceParametersChanged;

import java.util.ArrayList;
import java.util.List;

public class ServerLobby {
    private static final List<Track> availableTracks = new ArrayList<Track>();

    static {
        availableTracks.add(new Track("Beginner", "track2.tmx", "track2.png", 4, 1));
        availableTracks.add(new Track("Beginner", "track1.tmx", "track1.png", 4, 1));
        availableTracks.add(new Track("Intermediate", "track5.tmx", "track5.png", 3, 2));
        availableTracks.add(new Track("Intermediate", "track6.tmx", "track6.png", 3, 2));
        availableTracks.add(new Track("Intermediate", "track11.tmx", "track11.png", 3, 2));
        availableTracks.add(new Track("Advanced", "track4.tmx", "track4.png", 2, 2));
        availableTracks.add(new Track("Advanced", "track7.tmx", "track7.png", 3, 2));
        availableTracks.add(new Track("Advanced", "track10.tmx", "track10.png", 2, 2));
        availableTracks.add(new Track("Advanced", "track12.tmx", "track12.png", 3, 2));
        availableTracks.add(new Track("Professional", "track9.tmx", "track9.png", 2, 3));
        availableTracks.add(new Track("Professional", "track3.tmx", "track3.png", 2, 3));
        availableTracks.add(new Track("Professional", "track8.tmx", "track8.png", 2, 3));
    }

    private final Array<ServerPlayer> waitingPlayers;
    private final String raceCode;
    private final Race.Parameters parameters;
    private boolean acceptPlayers;

    public ServerLobby(String raceCode) {
        waitingPlayers = new Array<ServerPlayer>(false, 4);
        acceptPlayers = true;
        this.raceCode = raceCode;
        parameters = new Race.Parameters();
    }

    public void add(ServerPlayer player) {
        player.setRaceCode(raceCode);
        player.setColor(getNextColor());
        sendToAll(new PlayerJoined(player.getId(), player.getName(), player.getColor()));
        sendLoggedIn(player);
        waitingPlayers.add(player);
        if (waitingPlayers.size > 3) {
            acceptPlayers = false;
        }
    }

    public void sendLoggedIn(ServerPlayer player) {
        ArrayList<Player> players = new ArrayList<Player>();
        for (int i = 0; i < waitingPlayers.size; i++) {
            ServerPlayer p = waitingPlayers.get(i);
            if (p.getId() == player.getId()) continue;
            players.add(new Player(p.getId(), p.getName(), p.getColor()));
        }
        player.send(new LoggedIn(player.getId(), player.getName(), player.getColor(), players, availableTracks, player.getRaceCode(), parameters));
    }

    public void setLaps(int laps) {
        if (parameters.laps == laps) return;
        parameters.laps = laps;
        notifyParametersChanged();
    }

    private void notifyParametersChanged() {
        sendToAll(new RaceParametersChanged(parameters));
    }

    public void setTrack(String track) {
        if (parameters.track.equals(track)) return;
        boolean existsTrack = false;
        Track selectedTrack = null;
        for (Track t : availableTracks) {
            if (t.getTrackMap().equals(track)) {
                existsTrack = true;
                selectedTrack = t;
                break;
            }
        }
        if (!existsTrack) return;
        parameters.track = selectedTrack.getTrackMap();
        parameters.laps = selectedTrack.getLaps();
        notifyParametersChanged();
    }

    private void sendToAll(Object msg) {
        for (int i = 0; i < waitingPlayers.size; i++) {
            ServerPlayer player = waitingPlayers.get(i);
            player.send(msg);
        }
    }

    public boolean acceptPlayers() {
        return acceptPlayers;
    }

    public boolean isEmpty() {
        return waitingPlayers.isEmpty();
    }

    public void remove(ServerPlayer player) {
        waitingPlayers.removeValue(player, true);
        if (waitingPlayers.size < 4) {
            acceptPlayers = true;
        }
        sendToAll(new PlayerLeftLobby(player.getId()));
        player.setRaceCode(null);
    }

    public ServerRace startGame() {
        acceptPlayers = false;
        final ServerRace race = new ServerRace(parameters, waitingPlayers, raceCode);
        setPlayersRace(race);
        return race;
    }

    public void endGame() {
        setPlayersRace(null);
        acceptPlayers = true;
    }

    private void setPlayersRace(ServerRace race) {
        for (int i = 0; i < waitingPlayers.size; i++) {
            ServerPlayer player = waitingPlayers.get(i);
            player.setRace(race);
        }
    }

    public KartColor getNextColor() {
        for (KartColor color : KartColor.values()) {
            boolean isUsed = false;
            for (Player player : waitingPlayers) {
                if (player.getColor() == color) {
                    isUsed = true;
                    break;
                }
            }
            if (!isUsed) {
                return color;
            }
        }
        throw new RuntimeException("No Kart colors available");
    }

    @Override
    public String toString() {
        return "ServerLobby{" +
                raceCode +
                '}';
    }
}
