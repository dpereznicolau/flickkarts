package com.peritasoft.flickkarts;

import com.badlogic.gdx.utils.Array;


public class ClientLobby {
    private final Array<Listener> listeners;
    private final Array<Player> players;

    public ClientLobby() {
        this.listeners = new Array<Listener>(false, 2);
        players = new Array<Player>(false, 4);
    }

    public void addListener(final Listener listener) {
        if (listener == null) throw new IllegalArgumentException("listener cannot be null.");
        if (!listeners.contains(listener, true)) {
            listeners.add(listener);
        }
    }

    public int countPlayers() {
        return players.size;
    }

    public CharSequence getPlayerName(int index) {
        return players.get(index).getName();
    }

    public KartColor getPlayerColor(int index) {
        return players.get(index).getColor();
    }

    public void removePlayer(int id) {
        for (int i = 0; i < players.size; i++) {
            if (players.get(i).getId() == id) {
                players.removeIndex(i);
                notifyChange();
                return;
            }
        }
    }

    public Player addPlayer(int playerId, String playerName, KartColor kartColor) {
        Player player = new Player(playerId, playerName, kartColor);
        players.add(player);
        notifyChange();
        return player;
    }

    private void notifyChange() {
        for (int i = 0; i < listeners.size; i++) {
            listeners.get(i).lobbyUpdated();
        }
    }

    public interface Listener {
        void lobbyUpdated();
    }
}
