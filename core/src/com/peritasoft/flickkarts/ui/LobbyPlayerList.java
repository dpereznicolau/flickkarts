package com.peritasoft.flickkarts.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.peritasoft.flickkarts.ClientLobby;
import com.peritasoft.flickkarts.KartColor;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class LobbyPlayerList extends Widget implements ClientLobby.Listener {
    private final ClientLobby lobby;
    private final int alignment;
    private final EnumMap<KartColor, Texture> kartTextures;
    private LobbyPlayerListStyle style;
    private float prefWidth;
    private float prefHeight;
    private float itemWidth;

    public LobbyPlayerList(final ClientLobby lobby, final Skin skin, final EnumMap<KartColor, Texture> kartTextures) {
        this(lobby, skin.get(LobbyPlayerListStyle.class), kartTextures);
    }

    public LobbyPlayerList(final ClientLobby lobby, final LobbyPlayerListStyle style, final EnumMap<KartColor, Texture> kartTextures) {
        this.lobby = lobby;
        lobby.addListener(this);
        this.kartTextures = kartTextures;
        alignment = Align.center;

        setStyle(style);
        setSize(getPrefWidth(), getPrefHeight());
    }

    public void setStyle(final LobbyPlayerListStyle style) {
        if (style == null) {
            throw new IllegalArgumentException("style can not be null");
        }
        this.style = style;
        invalidateHierarchy();
    }

    @Override
    public void layout() {
        final BitmapFont font = style.font;
        final float fontHeight = font.getCapHeight() - font.getDescent() * 2;

        prefHeight = kartTextures.get(KartColor.RED).getHeight() + fontHeight + fontHeight / 4;

        Pool<GlyphLayout> layoutPool = Pools.get(GlyphLayout.class);
        GlyphLayout layout = layoutPool.obtain();
        itemWidth = kartTextures.get(KartColor.RED).getWidth();
        for (int i = 0; i < lobby.countPlayers(); i++) {
            layout.setText(font, lobby.getPlayerName(i));
            itemWidth = Math.max(layout.width + layout.width / 8 + 50, itemWidth);
        }
        layoutPool.free(layout);

        prefWidth = itemWidth * lobby.countPlayers() + (style.versus.getMinWidth() / 2) * (lobby.countPlayers() - 1);
        final Drawable background = style.background;
        if (background != null) {
            prefHeight += background.getTopHeight() + background.getBottomHeight();
            prefWidth += background.getLeftWidth() + background.getRightWidth();
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        validate();

        drawBackground(batch, parentAlpha);

        final BitmapFont font = style.font;
        final float fontHeight = font.getCapHeight() - font.getDescent() * 2;
        final Color fontColor = style.fontColor;
        final Drawable versus = style.versus;
        final float versusWidth = versus.getMinWidth();
        final float versusHeight = versus.getMinHeight();

        final Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

        float x = getX();
        float y = getY() + getHeight() - kartTextures.get(KartColor.RED).getHeight();
        float itemX = 0;

        final Drawable background = style.background;
        if (background != null) {
            final float leftWidth = background.getLeftWidth();
            x += leftWidth;
            y -= background.getTopHeight();
        }

        font.setColor(fontColor.r, fontColor.g, fontColor.b, color.a * parentAlpha);
        for (int i = 0; i < lobby.countPlayers(); i++) {
            Texture kartColor = kartTextures.get(lobby.getPlayerColor(i));
            batch.draw(kartColor, x + itemX + itemWidth / 2 - kartColor.getWidth() / 2, y);
            final CharSequence playerName = lobby.getPlayerName(i);
            drawName(batch, font, playerName, x + itemX, y,
                    itemWidth, fontHeight,
                    i == 0 ? style.backgroundOwn : style.backgroundOther);
            itemX += itemWidth + versusWidth / 2;
        }

        itemX = itemWidth;
        for (int i = 1; i < lobby.countPlayers(); i++) {
            versus.draw(batch, x + itemX - versusWidth / 4, y - fontHeight - fontHeight / 4, versusWidth, versusHeight);
            itemX += itemWidth + versusWidth / 2;
        }
    }

    protected void drawBackground(Batch batch, float parentAlpha) {
        if (style.background != null) {
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            style.background.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
    }

    protected void drawName(Batch batch, BitmapFont font, CharSequence name,
                            float x, float y, float width, float height, Drawable background) {
        background.draw(batch, x, y - height, width, height);
        font.draw(batch, name, x, y + font.getDescent() / 2, 0, name.length(), width, alignment, false, "...");
    }

    @Override
    public float getPrefWidth() {
        validate();
        return prefWidth;
    }

    @Override
    public float getPrefHeight() {
        validate();
        return prefHeight;
    }

    @Override
    public void lobbyUpdated() {
        final float oldPrefWidth = getPrefWidth();
        final float oldPrefHeight = getPrefHeight();

        invalidate();
        if (oldPrefHeight != getPrefHeight() || oldPrefWidth != getPrefWidth()) invalidateHierarchy();
    }

    public static class LobbyPlayerListStyle {
        public BitmapFont font;
        public Color fontColor = new Color(1, 1, 1, 1);
        public Drawable backgroundOwn;
        public Drawable backgroundOther;
        public Drawable versus;
        /**
         * Optional.
         */
        public Drawable background;

        public LobbyPlayerListStyle() {
        }

        public LobbyPlayerListStyle(BitmapFont font, Color fontColor, Drawable backgroundOwn, Drawable backgroundOther, Drawable versus) {
            this.font = font;
            this.fontColor = fontColor;
            this.backgroundOwn = backgroundOwn;
            this.backgroundOther = backgroundOther;
            this.versus = versus;
        }

        public LobbyPlayerListStyle(LobbyPlayerListStyle style) {
            this.font = style.font;
            this.fontColor.set(style.fontColor);
            this.backgroundOwn = style.backgroundOwn;
            this.backgroundOther = style.backgroundOther;
            this.versus = style.versus;
            this.background = style.background;
        }
    }
}
