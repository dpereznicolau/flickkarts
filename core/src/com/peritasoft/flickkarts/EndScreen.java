package com.peritasoft.flickkarts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.github.czyzby.websocket.WebSocket;

import java.util.List;

public class EndScreen extends FlickKartsScreen {
    WebSocket socket;
    List<String> classification;
    private final Texture background;
    private Music music;

    public EndScreen(FlickKarts game, WebSocket socket, List<String> classification) {
        super(game);
        this.socket = socket;
        this.classification = classification;
        background = new Texture("background.png");
        this.music = Gdx.audio.newMusic(Gdx.files.internal("music/menu.mp3"));
        this.music.setLooping(true);
        this.music.setVolume(0.20f);
        this.music.play();
        setupLayout(game.getSkin());
    }

    private void setupLayout(Skin skin) {
        final TextureRegion tr = new TextureRegion(background);
        tr.flip(true, false);
        final Image backgroundImage = new Image(new TextureRegionDrawable(tr), Scaling.fill);
        final Container<Image> background = new Container<Image>(backgroundImage);
        background.setFillParent(true);
        background.fill();
        addActor(background);

        final Table table = new Table();
        table.setFillParent(true);
        addActor(table);

        Table playerPodium = new Table(skin);
//        playerPodium.setDebug(true);
        table.add(playerPodium).expandX().expandY().padLeft(100f).left();
        final Label playerPodiumLabel = new Label("Race result", skin);
        playerPodiumLabel.setAlignment(Align.center);
        playerPodium.add(playerPodiumLabel).top().expandX().colspan(2).row();
        int position = 1;
        for (String playerName : classification) {
            playerPodium.add(new Label(" " + position + " ", skin, "rank_background_pos")).expandX().fill().right().padTop(8f);
            playerPodium.add(new Label(playerName, skin, "rank_background_name")).expandX().fill().padLeft(-1f).padTop(8f).row();
            position++;
        }
        table.row();

        final TextButton exit = new TextButton("Exit to Menu", skin);
        exit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                final MenuScreen menu = new MenuScreen(game, music);
                music = null;
                game.replaceScreen(menu);
            }
        });

        final TextButton rematch = new TextButton("Rematch", skin);
        rematch.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                final ClientLobbyScreen client = new ClientLobbyScreen(game, game.getRaceCode(), socket, music);
                socket = null;
                music = null;
                game.replaceScreen(client);
            }
        });

        final Table buttonBar = new Table(skin);
        buttonBar.pad(0, 30, 20, 30);
        buttonBar.add(exit).expandX().left();
        buttonBar.add(rematch).expandX().right();
        table.add(buttonBar).fillX().expandX();
    }

    @Override
    public void hide() {
        super.hide();
        if (music != null) {
            music.dispose();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        if (socket != null) {
            socket.close();
        }
    }
}
