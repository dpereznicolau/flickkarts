package com.peritasoft.flickkarts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class FlickKartsScreen extends Stage implements Screen {

    protected final FlickKarts game;

    protected FlickKartsScreen(final FlickKarts game) {
        super(new ExtendViewport(1280, 720), game.getBatch());
        this.game = game;
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public boolean keyDown(int keyCode) {
        switch (keyCode) {
            case Input.Keys.BACK:
            case Input.Keys.ESCAPE:
                goBack();
                return true;
            default:
                return super.keyDown(keyCode);
        }
    }

    protected void goBack() {
    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0, 1);
        act(delta);
        draw();
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width, height, true);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }
}
