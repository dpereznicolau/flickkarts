package com.peritasoft.flickkarts;

import com.badlogic.gdx.physics.box2d.*;
import com.peritasoft.flickkarts.items.Item;

public class ItemBox {
    private final Body body;
    private final Race race;

    public ItemBox(float x, float y, World world, Race race) {
        body = createBox(x, y, world);
        this.race = race;
    }

    private Body createBox(float posX, float posY, World world) {
        BodyDef def = new BodyDef();
        def.position.set(posX, posY);
        def.type = BodyDef.BodyType.StaticBody;
        Body body = world.createBody(def);
        body.setUserData(this);
        FixtureDef fixture = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(50 * FlickKarts.UNIT_SCALE, 50 * FlickKarts.UNIT_SCALE);
        fixture.shape = shape;
        fixture.isSensor = true;
        body.createFixture(fixture);
        shape.dispose();
        return body;
    }

    public void delete() {
        race.deleteItemBox(this);
    }

    public Body getBody() {
        return body;
    }

    public float getX() {
        return body.getPosition().x;
    }

    public float getY() {
        return body.getPosition().y;
    }

    public void dropItem(Kart kart) {
        race.generateRandomItem(kart);
        delete();
    }
}
