package com.peritasoft.flickkarts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.*;

public class RaceContactListener implements ContactListener {
    private SfxPlayer sfxPlayer;

    public RaceContactListener(SfxPlayer sfxPlayer) {
        this.sfxPlayer = sfxPlayer;
    }

    private static Kart getKart(final Contact contact) {
        final Fixture fixtureA = contact.getFixtureA();
        final Fixture fixtureB = contact.getFixtureB();
        if (fixtureA == null || fixtureB == null) {
            return null;
        }

        final Object maybeKart = (fixtureA.isSensor() ? fixtureB : fixtureA).getBody().getUserData();
        if (!(maybeKart instanceof Kart)) {
            return null;
        }
        return (Kart) maybeKart;
    }

    private static Object getSensor(final Contact contact) {
        final Fixture fixtureA = contact.getFixtureA();
        final Fixture fixtureB = contact.getFixtureB();
        if (fixtureA == null || fixtureB == null) {
            return null;
        }
        return (fixtureA.isSensor() ? fixtureA : fixtureB).getBody().getUserData();
    }

    private void playSfx(Contact contact) {
        final Fixture fixtureA = contact.getFixtureA();
        final Fixture fixtureB = contact.getFixtureB();
        if (fixtureA == null || fixtureB == null) {
            return;
        }

        final Object objectA = fixtureA.getBody().getUserData();
        final Object objectB = fixtureB.getBody().getUserData();

        //collision
        if ((objectA instanceof Kart && !fixtureB.isSensor())
        || (objectB instanceof Kart && !fixtureA.isSensor())) {
            if (sfxPlayer != null) {
                sfxPlayer.playSfxFlick();
            }
        }
    }

    @Override
    public void beginContact(Contact contact) {
        playSfx(contact);
        Kart kart = getKart(contact);
        if (kart == null) {
            return;
        }

        final Object sensor = getSensor(contact);
        if (sensor == null) {
            return;
        }

        if (sensor instanceof Race) {
            ((Race) sensor).crossedFinishLine(kart);
        } else if (sensor instanceof Itinerary.Checkpoint) {
            Itinerary.Checkpoint checkpoint = (Itinerary.Checkpoint) sensor;
            if (checkpoint.number - kart.getCurrentSector() == 1) {
                kart.nextSector();
            }
        } else if (sensor instanceof Terrain) {
            float friction = ((Terrain) sensor).getFriction();
            System.out.println("Soc al terreny amb friccio: " + friction);
            kart.addFriction(friction);
        } else if (sensor instanceof ItemBox) {
            ((ItemBox) sensor).dropItem(kart);
        }
    }

    @Override
    public void endContact(Contact contact) {
        Kart kart = getKart(contact);
        if (kart == null) {
            return;
        }

        final Object sensor = getSensor(contact);
        if (sensor == null) {
            return;
        }

        if (sensor instanceof Terrain) {
            float friction = ((Terrain) sensor).getFriction();
            System.out.println("Surto del terreny amb friccio: " + friction);
            kart.subFriction(friction);
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }
}