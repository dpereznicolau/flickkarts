package com.peritasoft.flickkarts;

public enum KartColor {
    BLACK("kart_black.png"),
    GREEN("kart_green.png"),
    RED("kart_red.png"),
    YELLOW("kart_yellow.png");

    private String texturePath;

    KartColor(String texture) {
        this.texturePath = texture;
    }

    public String getTexturePath() {
        return texturePath;
    }
}
