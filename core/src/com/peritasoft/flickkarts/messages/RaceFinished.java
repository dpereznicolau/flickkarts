package com.peritasoft.flickkarts.messages;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("FieldMayBeFinal")
public class RaceFinished {
    private ArrayList<String> classification;

    @SuppressWarnings("unused")
    public RaceFinished() {
        this(null);
    }

    public RaceFinished(ArrayList<String> classification) {
        this.classification = classification;
    }

    public List<String> getClassification() {
        return classification;
    }

    @Override
    public String toString() {
        return "RaceFinished{" +
                "classification=" + classification +
                '}';
    }
}
