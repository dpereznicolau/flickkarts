package com.peritasoft.flickkarts.messages;

@SuppressWarnings("FieldMayBeFinal")
public class StartGame {

    @SuppressWarnings("unused")
    public StartGame() {
    }

    @Override
    public String toString() {
        return "StartGame{" +
                '}';
    }
}
