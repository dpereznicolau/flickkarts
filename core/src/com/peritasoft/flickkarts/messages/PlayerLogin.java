package com.peritasoft.flickkarts.messages;

@SuppressWarnings("FieldMayBeFinal")
public class PlayerLogin {
    private String playerName;
    private String raceCode;

    @SuppressWarnings("unused")
    public PlayerLogin() {
        this("Ghost", null);
    }

    public PlayerLogin(String playerName, String raceCode) {
        this.playerName = playerName;
        this.raceCode = raceCode;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getRaceCode() {
        return raceCode;
    }

    @Override
    public String toString() {
        return "PlayerLogin{" +
                "playerName='" + playerName + '\'' +
                ", raceCode=" + (raceCode == null ? "null" : '\'' + raceCode + '\'') +
                '}';
    }
}
