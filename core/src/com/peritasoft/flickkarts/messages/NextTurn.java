package com.peritasoft.flickkarts.messages;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("FieldMayBeFinal")
public class NextTurn {
    private int nextPlayer;
    private ArrayList<KartState> kartStates;
    private int currentRound;

    @SuppressWarnings("unused")
    public NextTurn() {
        nextPlayer = -1;
        kartStates = new ArrayList<KartState>();
        currentRound = 0;
    }

    public NextTurn(int nextPlayer, ArrayList<KartState> kartStates, int currentRound) {
        this.nextPlayer = nextPlayer;
        this.kartStates = kartStates;
        this.currentRound = currentRound;
    }

    public int getNextPlayer() {
        return nextPlayer;
    }

    public List<KartState> getKartStates() {
        return kartStates;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    @Override
    public String toString() {
        return "NextTurn{" +
                "nextPlayer=" + nextPlayer +
                ", kartStates=" + kartStates +
                ", currentRound=" + currentRound +
                '}';
    }
}
