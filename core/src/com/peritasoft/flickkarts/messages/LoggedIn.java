package com.peritasoft.flickkarts.messages;

import com.peritasoft.flickkarts.KartColor;
import com.peritasoft.flickkarts.Player;
import com.peritasoft.flickkarts.Race;
import com.peritasoft.flickkarts.Track;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("FieldMayBeFinal")
public class LoggedIn {
    private int playerId;
    private String playerName;
    private KartColor kartColor;
    private ArrayList<Player> players;
    private List<Track> availableTracks;
    private String raceCode;
    private Race.Parameters raceParameters;

    @SuppressWarnings("unused")
    public LoggedIn() {
        this(-1, null, null, null, null, null, null);
    }

    public LoggedIn(int playerId, String playerName, KartColor kartColor, ArrayList<Player> players, List<Track> availableTracks, String raceCode, final Race.Parameters raceParameters) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.kartColor = kartColor;
        this.players = players;
        this.availableTracks = availableTracks;
        this.raceCode = raceCode;
        this.raceParameters = raceParameters;
    }

    public int getPlayerId() {
        return playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public String getRaceCode() {
        return raceCode;
    }

    public List<Track> getAvailableTracks() {
        return availableTracks;
    }

    public Race.Parameters getRaceParameters() {
        return raceParameters;
    }

    public KartColor getKartColor() {
        return kartColor;
    }

    @Override
    public String toString() {
        return "LoggedIn{" +
                "playerId=" + playerId +
                ", playerName='" + playerName + '\'' +
                ", kartColor='" + kartColor + '\'' +
                ", players=" + players +
                ", availableTracks=" + availableTracks +
                ", raceCode='" + raceCode + '\'' +
                ", raceParameters=" + raceParameters +
                '}';
    }
}
