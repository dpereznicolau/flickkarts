package com.peritasoft.flickkarts.messages;
import com.peritasoft.flickkarts.items.Item;

public class PickItem {
    private int id;
    private Item item;

    public PickItem() {
        this(-1, null);
    }

    public PickItem(int id, Item item) {
        this.id = id;
        this.item = item;
    }

    public int getId() {
        return id;
    }

    public Item getItem() {
        return item;
    }

    @Override
    public String toString() {
        return "PickItem{" +
                "id=" + id +
                ", item=" + item +
                '}';
    }
}
