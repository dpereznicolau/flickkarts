package com.peritasoft.flickkarts.messages;

import com.badlogic.gdx.math.Vector2;

@SuppressWarnings("FieldMayBeFinal")
public class Flick {
    private int id;
    private Vector2 impulse;

    @SuppressWarnings("unused")
    public Flick() {
        this(-1, Vector2.Zero);
    }

    public Flick(int id, Vector2 impulse) {
        this.id = id;
        this.impulse = impulse;
    }

    public Flick(Vector2 impulse) {
        this(-1, impulse);
    }

    public int getId() {
        return id;
    }

    public Vector2 getImpulse() {
        return impulse;
    }

    @Override
    public String toString() {
        return "Flick{" +
                "id=" + id +
                ", impulse=" + impulse +
                '}';
    }
}
