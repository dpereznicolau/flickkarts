package com.peritasoft.flickkarts.messages;

import com.peritasoft.flickkarts.Race;

@SuppressWarnings("FieldMayBeFinal")
public class RaceParametersChanged {
    private Race.Parameters parameters;

    @SuppressWarnings("unused")
    public RaceParametersChanged() {
        this(null);
    }

    public RaceParametersChanged(final Race.Parameters parameters) {
        this.parameters = parameters;
    }

    public Race.Parameters getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        return "RaceParametersChanged{" +
                "parameters=" + parameters +
                '}';
    }
}
