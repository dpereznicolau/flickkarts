package com.peritasoft.flickkarts.messages;

@SuppressWarnings("FieldMayBeFinal")
public class ChangeTrack {
    private String track;

    @SuppressWarnings("unused")
    public ChangeTrack() {
        this(null);
    }

    public ChangeTrack(String track) {
        this.track = track;
    }

    public String getTrack() {
        return track;
    }

    @Override
    public String toString() {
        return "ChangeTrack{" +
                "track='" + track + '\'' +
                '}';
    }
}
