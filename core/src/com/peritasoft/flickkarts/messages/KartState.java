package com.peritasoft.flickkarts.messages;

import com.badlogic.gdx.math.Vector2;

@SuppressWarnings("FieldMayBeFinal")
public class KartState {
    private int id;
    private Vector2 position;
    private float angle;
    private int sector;
    private int lap;

    @SuppressWarnings("unused")
    public KartState() {
        this(-1, Vector2.Zero, 0f, 0, 0);
    }

    public KartState(int id, final Vector2 position, float angle, int sector, int lap) {
        this.id = id;
        this.position = position;
        this.angle = angle;
        this.sector = sector;
        this.lap = lap;
    }

    public int getId() {
        return id;
    }

    public Vector2 getPosition() {
        return position;
    }

    public float getAngle() {
        return angle;
    }

    public int getLap() {
        return lap;
    }

    public int getSector() {
        return sector;
    }

    @Override
    public String toString() {
        return "KartState{" +
                "id=" + id +
                ", position=" + position +
                ", angle=" + angle +
                ", sector=" + sector +
                ", lap=" + lap +
                '}';
    }
}
