package com.peritasoft.flickkarts.messages;

@SuppressWarnings("FieldMayBeFinal")
public class KartClassified {
    public int id;

    @SuppressWarnings("unused")
    public KartClassified() {
        this(-1);
    }

    public KartClassified(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "KartClassified{" +
                "id=" + id +
                '}';
    }
}
