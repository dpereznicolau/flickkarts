package com.peritasoft.flickkarts.messages;

@SuppressWarnings("FieldMayBeFinal")
public class LoginError {
    private ErrorCode code;

    @SuppressWarnings("unused")
    public LoginError() {
        this(ErrorCode.NOT_ERROR);
    }

    public LoginError(ErrorCode code) {
        this.code = code;
    }

    public ErrorCode getCode() {
        return code;
    }

    public enum ErrorCode {
        LOBBY_NOT_EXIST,
        LOBBY_NOT_AVAILABLE,
        SERVER_NOT_AVAILABLE,
        NOT_ERROR,
    }

    @Override
    public String toString() {
        return "LoginError{" +
                code +
                '}';
    }
}
