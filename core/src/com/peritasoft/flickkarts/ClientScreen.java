package com.peritasoft.flickkarts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.github.czyzby.websocket.WebSocket;
import com.peritasoft.flickkarts.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ClientScreen extends FlickKartsScreen implements GestureDetector.GestureListener, Client.Listener, Race.Listener {
    public static final float VIEWPORT_WIDTH = 3f;
    public static final float VIEWPORT_HEIGHT = 1.5f;
    public static final float DEFAULT_FONT_SCALE = 3f;
    public static final float MAX_IMPULSE = 640 * FlickKarts.UNIT_SCALE;
    private static final Logger log = LoggerFactory.getLogger(ClientScreen.class);
    private final OrthogonalTiledMapRenderer tiledMapRenderer;
    private final EnumMap<KartColor, Sprite> kartSprites;
    private final Sprite itemBoxSprite;
    private final Table tableClassification;
    private final Label[] positions;
    private final Label[] playerNames;
    private final Label[] laps;
    private final Label round;
    private final Vector2 tmpPoint;
    private final Batch batch;
    private final OrthographicCamera camera;
    private final Box2DDebugRenderer renderer;
    private final ShapeRenderer shapeRenderer;
    private final FitViewport viewPort;
    private final Client client;
    private final Player player;
    private final Race race;
    private WebSocket socket;
    private float cameraZoom = -1f;
    private boolean followPlayer = true;
    private Kart kart;
    private boolean flicking;
    private final MusicPlayer musicPlayer;
    private final SfxPlayer sfxPlayer;

    public ClientScreen(FlickKarts game
            , final Race.Parameters raceParameters
            , Iterable<? extends KartState> kartsState
            , Map<String, String> playerMap
            , Map<String, KartColor> playerColorMap
            , final WebSocket socket
            , Player player
    ) {
        super(game);
        this.socket = socket;
        this.player = player;
        client = new Client(this);
        socket.addListener(client);
        batch = game.getBatch();
        renderer = new Box2DDebugRenderer();
        shapeRenderer = new ShapeRenderer();

        camera = new OrthographicCamera();
        camera.zoom = 0.25f;
        viewPort = new FitViewport(VIEWPORT_WIDTH, VIEWPORT_HEIGHT, camera);
        setViewport(new FitViewport(VIEWPORT_WIDTH / FlickKarts.UNIT_SCALE, VIEWPORT_HEIGHT / FlickKarts.UNIT_SCALE));

        tmpPoint = new Vector2();

        musicPlayer = new MusicPlayer(game);
        musicPlayer.randomize();
        musicPlayer.playNextTrack();

        sfxPlayer = new SfxPlayer(game);

        kartSprites = new EnumMap<KartColor, Sprite>(KartColor.class);
        for (KartColor kc : KartColor.values()) {
            kartSprites.put(kc, new Sprite(new Texture(kc.getTexturePath())));
        }

        final Texture itemButtonTexture = new Texture("itembox.png");
        itemBoxSprite = new Sprite(itemButtonTexture);
        itemBoxSprite.setScale(FlickKarts.UNIT_SCALE);

        //Boto per activar els items, deshabilitat fins que tinguem items implementats
//        final Table table = new Table(game.getSkin());
//        table.setFillParent(true);
//        addActor(table);
//        final Texture bursterButtonTexture = new Texture("burster.png");
//        bursterButtonTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
//        ImageButton itemButton = new ImageButton(new TextureRegionDrawable(bursterButtonTexture));
//        itemButton.setScale(4);
//        itemButton.setTransform(true);
//        itemButton.setDebug(true);
//        itemButton.addListener(new ChangeListener() {
//            @Override
//            public void changed(ChangeEvent event, Actor actor) {
//                useItem();
//            }
//        });
//        table.add(itemButton).expand().bottom();

        final Table table = new Table();
        table.top();
        table.setFillParent(true);
        addActor(table);
        Table subTable = new Table();
        tableClassification = new Table(game.getSkin());
        subTable.add(tableClassification).expandX().left();
        round = new Label("round", game.getSkin(), "rank_background_name");
        round.setFontScale(DEFAULT_FONT_SCALE);
        subTable.add(round).expandX().top().right();
        table.add(subTable).top().fillX().expandX().padRight(25f).padLeft(25f).padTop(25f);

        positions = new Label[playerMap.size()];
        playerNames = new Label[playerMap.size()];
        laps = new Label[playerMap.size()];
        for (int i=0; i<positions.length; i++) {
            positions[i] = new Label(" " + (i + 1) + " ", game.getSkin(), "rank_background_pos");
            positions[i].setFontScale(DEFAULT_FONT_SCALE);
            playerNames[i] = new Label("player", game.getSkin(), "rank_background_name");
            playerNames[i].setFontScale(DEFAULT_FONT_SCALE);
            laps[i] = new Label("    0 ", game.getSkin(), "rank_background_name");
            laps[i].setFontScale(DEFAULT_FONT_SCALE);
            tableClassification.add(positions[i]).expandX().fill().right().padTop(10f);
            tableClassification.add(playerNames[i]).expandX().fill().padLeft(-1f).padTop(10f);
            tableClassification.add(laps[i]).expandX().fill().padLeft(-30f).padTop(10f).row();
        }

        //create race
        Array<Player> players = new Array<Player>(false, playerMap.size());
        for (KartState kartState : kartsState) {
            players.add(new Player(kartState.getId(), playerMap.get(String.valueOf(kartState.getId())), playerColorMap.get(String.valueOf(kartState.getId()))));
        }
        race = new Race(raceParameters, players, new TmxMapLoader(), this, sfxPlayer);
        tiledMapRenderer = new OrthogonalTiledMapRenderer(race.getMap(), FlickKarts.UNIT_SCALE, batch);
        kart = race.findKartById(player.getId());
        adjustKarts(kartsState);
    }

    @Override
    public void show() {
        final GestureDetector gestureDetector = new GestureDetector(this);
        gestureDetector.setTapSquareSize(1);
        Gdx.input.setInputProcessor(new InputMultiplexer(gestureDetector, this));
    }

    public void act(float delta) {
        super.act(delta);
        if (race != null) race.update();
        translateCameraToCurrentKart(delta);
    }

    public void draw() {
        super.draw();
        if (race == null) {
            return;
        }

        viewPort.apply();
        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        for (Kart k : race.getKarts()) {
            Sprite kartSp = kartSprites.get(k.getPlayerColor());
            kartSp.setScale(FlickKarts.UNIT_SCALE);
            kartSp.setOriginBasedPosition(k.getPosition().x, k.getPosition().y);
            kartSp.setRotation(k.getAngle());
            kartSp.draw(batch);
        }

        for (ItemBox box : race.getItemBoxes()) {
            itemBoxSprite.setOriginBasedPosition(box.getX(), box.getY());
            itemBoxSprite.draw(batch);
        }

        getViewport().apply(true);
        batch.setProjectionMatrix(getCamera().combined);
        Kart k = race.getCurrentKart();
        if (k != null && !k.isMoving()) {
            batch.end();
            Gdx.gl.glLineWidth(5f);
            shapeRenderer.setProjectionMatrix(camera.combined);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.circle(k.getPosition().x, k.getPosition().y, Kart.RADIUS + 0.0025f, 100);
            shapeRenderer.end();
            Gdx.gl.glLineWidth(1f);
            batch.begin();
        }
        //marcar kart actiu actualment, caldria mirar com ho fem (l'actual ^^^^ es molt cutre)
//        if (kart != null) {
//            Vector2 labelPosition = new Vector2(kart.getPosition());
//            labelPosition.sub(0, Kart.RADIUS);
//            labelPosition = viewPort.project(labelPosition);
//            labelPosition.y = Gdx.graphics.getHeight() - labelPosition.y;
//            labelPosition = screenToStageCoordinates(labelPosition);
//            font.getData().setScale(DEFAULT_FONT_SCALE / camera.zoom * 0.25f);
//            font.draw(batch, "^^^^", labelPosition.x - 16, labelPosition.y, 32, Align.center, false);
//        }


        List<Kart> classification = race.getClassification();
        //treure etiquetes sobrants
        for (int i = 0; i < positions.length - classification.size() ; i++) {
            tableClassification.removeActor(positions[positions.length - i - 1]);
            tableClassification.removeActor(playerNames[playerNames.length - i - 1]);
            tableClassification.removeActor(laps[laps.length - i - 1]);
        }

        for (int i = 0; i < classification.size(); i++) {
            playerNames[i].setText("  " + classification.get(i).getPlayerName() + "     ");
            laps[i].setText("    " + classification.get(i).getLap() + "  ");
        }
        round.setText("   Round " + race.getCurrentRound() + "   ");

        batch.end();

        //dibuixar linies debug
//        renderer.render(race.getWorld(), camera.combined);
        super.draw();
    }

    private void translateCameraToCurrentKart(float delta) {
        Kart currentKart = race.getCurrentKart();
        if (currentKart != null && followPlayer) {
            final float halfMapWidth = race.getMapWidth() / 2f;
            final float halfMapHeight = race.getMapHeight() / 2f;
            final float halfWorldWidth = viewPort.getWorldWidth() / 2.0f * camera.zoom;
            final float halfWorldHeight = viewPort.getWorldHeight() / 2.0f * camera.zoom;
            final Vector2 kartPosition = currentKart.getPosition();
            final float cameraPosX = MathUtils.clamp(kartPosition.x,
                    Math.min(halfMapWidth, halfWorldWidth),
                    Math.max(halfMapWidth, race.getMapWidth() - halfWorldWidth)
            ) - camera.position.x;
            final float cameraPosY = MathUtils.clamp(kartPosition.y,
                    Math.min(halfMapHeight, halfWorldHeight),
                    Math.max(halfMapHeight, race.getMapHeight() - halfWorldHeight)
            ) - camera.position.y;
            final float cameraIncPos = delta * 3f; //camera speed
            tmpPoint.set(cameraPosX, cameraPosY).nor();
            tmpPoint.set(
                    Math.min(Math.abs(tmpPoint.x * cameraIncPos), Math.abs(cameraPosX)) * Math.signum(cameraPosX),
                    Math.min(Math.abs(tmpPoint.y * cameraIncPos), Math.abs(cameraPosY)) * Math.signum(cameraPosY)
            );
            camera.position.add(tmpPoint.x, tmpPoint.y, 0.0f);
        }
        camera.update();
    }

    @Override
    public void goBack() {
        game.setScreen(new OptionsScreen(game, this, musicPlayer));
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewPort.update(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
        if (race != null) {
            race.dispose();
        }
        for (Sprite kc : kartSprites.values()) {
            kc.getTexture().dispose();
        }
        renderer.dispose();
        tiledMapRenderer.dispose();
        if (socket != null) {
            socket.close();
        }
        musicPlayer.dispose();
        sfxPlayer.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (super.keyDown(keycode)) return true;
        switch (keycode) {
            case Input.Keys.M:
                musicPlayer.muteUnmute();
                return true;
            case Input.Keys.U:
                useItem();
                return true;
            case Input.Keys.SPACE:
                musicPlayer.pauseResumeMusic();
                return true;
            case Input.Keys.N:
                musicPlayer.playNextTrack();
                return true;
        }
        return false;
    }

    private void useItem() {
        socket.send(new UseItem());
        kart.useItem();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (super.touchDown(screenX, screenY, pointer, button)) return true;
        if (race == null || !race.isCurrentKart(player.getId()) || kart.isMoving()) return false;
        tmpPoint.set(screenX, screenY);
        viewPort.unproject(tmpPoint);
        if (button == Input.Buttons.LEFT && race.isTouchingKart(kart, tmpPoint)) {
            flicking = true;
            return true;
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (super.touchUp(screenX, screenY, pointer, button)) return true;
        flicking = false;
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        if (super.scrolled(amountX, amountY)) return true;
        float zoomFact = amountY * -0.05f;
        zoomInOut(zoomFact);
        cameraZoom = -1;
        return true;
    }

    private void zoomInOut(float zoomFact) {
        float maxHZoom = race.getMapHeight() / viewPort.getWorldHeight();
        float maxWZoom = race.getMapWidth() / viewPort.getWorldWidth();
        float maxZoom = Math.max(maxHZoom, maxWZoom);

        if (cameraZoom < 0) cameraZoom = camera.zoom;
        camera.zoom = MathUtils.clamp(cameraZoom - zoomFact, 0.15f, maxZoom);
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        if (count >= 2) {
            followPlayer = true;
            camera.zoom = 0.25f;
            return true;
        }
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        if (!flicking) return false;
        sfxPlayer.playSfxFlick();
        tmpPoint.set(velocityX, -velocityY);
        Vector2 impulse = tmpPoint.scl(FlickKarts.UNIT_SCALE).limit(MAX_IMPULSE).scl(0.03f);
        followPlayer = true;
        kart.flick(impulse);
        socket.send(new Flick(impulse));
        flicking = false;
        return true;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        if (flicking) return false;
        followPlayer = false;
        float factor = (0.0005f / 0.25f) * camera.zoom;
        camera.position.add(deltaX * -factor, deltaY * factor, 0f);
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        float zoomFact = (distance - initialDistance) * 0.0005f;
        zoomInOut(zoomFact);
        return true;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {
        cameraZoom = -1f;
    }

    @Override
    public void onFlick(Flick msg) {
        if (msg.getId() != player.getId()) {
            flicking = false;
            race.flick(msg.getId(), msg.getImpulse());
        }
        followPlayer = true;
    }

    @Override
    public void onNextTurn(final NextTurn msg) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                race.setCurrentKart(msg.getNextPlayer());
                race.setCurrentRound(msg.getCurrentRound());
                adjustKarts(msg.getKartStates());
            }
        });
    }

    private void adjustKarts(final Iterable<? extends KartState> kartStates) {
        for (KartState state : kartStates) {
            race.adjustKart(state.getId(), state.getPosition(), state.getAngle(), state.getSector(), state.getLap());
            log.debug("Kart {} on lap {}", state.getId(), state.getLap());
        }
    }

    @Override
    public void onPlayerLeftRace(final PlayerLeftRace msg) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                race.removeKart(msg.getPlayerId());
            }
        });
    }

    @Override
    public void onRaceFinished(final RaceFinished msg) {
        log.debug("Race Finished");
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                socket.removeListener(client);
                EndScreen endScreen = new EndScreen(game, socket, msg.getClassification());
                socket = null;
                game.replaceScreen(endScreen);
            }
        });
    }

    @Override
    public void onKartClassified(final KartClassified msg) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (kart != null && msg.id == kart.getId()) {
                    kart = null;
                }
                race.classifyKart(msg.id);
            }
        });
    }

    @Override
    public void onPickItem(PickItem msg) {
        if (kart.getId() == msg.getId()) {
            kart.pickItem(msg.getItem());
        }
    }

    @Override
    public void generateRandomItem(Kart kart) {

    }
}
