package com.peritasoft.flickkarts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.github.czyzby.websocket.WebSocket;
import com.github.czyzby.websocket.data.WebSocketException;
import com.github.czyzby.websocket.net.ExtendedNet;
import com.peritasoft.flickkarts.messages.*;
import com.peritasoft.flickkarts.ui.LobbyPlayerList;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ClientLobbyScreen extends FlickKartsScreen implements Client.LobbyListener {
    private final ClientLobby lobby;
    private final Client client;
    private final Map<Integer, String> tracks;
    private final Texture background;
    private final NinePatch backgroundSelected;
    private final EnumMap<KartColor, Texture> kartTextures;
    private WebSocket socket;
    private Player player;
    private Label code;
    private Table thumbsHGroup;
    private ScrollPane thumbsScroll;
    private int selectedTrack;
    private Music music;

    public ClientLobbyScreen(FlickKarts game, Music music) {
        this(game, null, music);
    }

    public ClientLobbyScreen(FlickKarts game, String raceCode, Music music) {
        this(game, raceCode, null, music);
    }

    public ClientLobbyScreen(FlickKarts game, String raceCode, WebSocket socket, Music music) {
        super(game);
        lobby = new ClientLobby();
        client = new Client(this);
        game.setRaceCode(raceCode);
        this.socket = socket;
        tracks = new HashMap<Integer, String>();
        background = new Texture("background.png");
        TextureAtlas atlas = new TextureAtlas("uiskin.atlas");
        backgroundSelected =  atlas.createPatch("button-yellow");
        kartTextures = new EnumMap<KartColor, Texture>(KartColor.class);
        for (KartColor kc : KartColor.values()) {
            kartTextures.put(kc, new Texture(kc.getTexturePath()));
        }
        selectedTrack = 0;
        if (music == null) {
            music = Gdx.audio.newMusic(Gdx.files.internal("music/menu.mp3"));
            music.setLooping(true);
            music.setVolume(0.25f);
            music.play();
        } else {
            this.music = music;
        }
        setupLayout(game.getSkin());
    }

    private void setupLayout(Skin skin) {
        final Image backgroundImage = new Image(new TextureRegionDrawable(background), Scaling.fill);
        final Container<Image> background = new Container<Image>(backgroundImage);
        background.setFillParent(true);
        background.fill();
        addActor(background);

        final Table table = new Table(skin);
//        table.setDebug(true);
        table.setFillParent(true);
        addActor(table);

        final Table buttonBar = new Table(skin);
        buttonBar.background("panel");
        buttonBar.pad(0, 20, 0, 20);

        final TextButton backButton = new TextButton("Back", skin);
        buttonBar.add(backButton).expandX().left();
        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                goBack();
            }
        });

        code = new Label("Code: " + game.getRaceCode(), skin, "tooltip");
        buttonBar.add(code).top().center();

        final TextButton playButton = new TextButton("Play", skin);
        buttonBar.add(playButton).expandX().right();
        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                socket.send(new StartGame());
            }
        });


        table.add(buttonBar).top().fillX().expandX();
        table.row();

        final LobbyPlayerList playerList = new LobbyPlayerList(lobby, skin, kartTextures);
        table.add(playerList).row();

        Table mapVGroup = new Table(skin);
        mapVGroup.background("panel");
        mapVGroup.pad(0, 20, 0, 20);
        mapVGroup.columnDefaults(0).expandX().fillX();
        table.add(mapVGroup).expand().fill();

        thumbsHGroup = new Table(skin);
        thumbsScroll = new ScrollPane(thumbsHGroup, skin);
        mapVGroup.add(thumbsScroll).fill().expand();
    }

    @Override
    protected void goBack() {
        super.goBack();
        final MenuScreen menu = new MenuScreen(game, music);
        music = null;
        game.replaceScreen(menu);
    }

    public void connect(String address, int port) {
        socket = ExtendedNet.getNet().newWebSocket(address, port);
        socket.setSerializeAsString(true);
        socket.addListener(client);
        socket.connect();
    }

    @Override
    public void show() {
        super.show();
        try {
            if (socket == null) {
//            connect("davidp.eu", 8080);
                connect("192.168.1.40", 8080);
//            connect("localhost", 8080);
            } else {
                socket.addListener(client);
                socket.send(new Rematch());
            }
        } catch (WebSocketException e) {
            final MenuScreen menu = new MenuScreen(game, music, new LoginError(LoginError.ErrorCode.SERVER_NOT_AVAILABLE));
            music = null;
            game.replaceScreen(menu);
        }
    }

    @Override
    public void hide() {
        super.hide();
        if (socket != null) {
            socket.close();
            socket = null;
        }
        if (music != null) {
            music.dispose();
        }
    }

    @Override
    public void dispose() {
        background.dispose();
        for (Texture kc : kartTextures.values()) {
            kc.dispose();
        }
        super.dispose();
    }

    @Override
    public PlayerLogin onConnect() {
        return new PlayerLogin(game.getPlayerName(), game.getRaceCode());
    }

    @Override
    public void onGameStarted(final GameStarted msg) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                socket.removeListener(client);
                ClientScreen client = getClientScreen(
                        msg.getRaceParameters()
                        , msg.getKartsState()
                        , msg.getPlayers()
                        , msg.getPlayersColor()
                );
                socket = null; // replaceScreen will call hide and we can’t just stop everything.
                game.replaceScreen(client);
            }
        });
    }

    @Override
    public void onLoggedIn(final LoggedIn msg) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                player = lobby.addPlayer(msg.getPlayerId(), msg.getPlayerName(), msg.getKartColor());
                for (Player p : msg.getPlayers()) {
                    lobby.addPlayer(p.getId(), p.getName(), p.getColor());
                }
                game.setRaceCode(msg.getRaceCode());
                code.setText("Code: " + game.getRaceCode());

                thumbsHGroup.clearChildren();
                final java.util.List<Track> availableTracks = msg.getAvailableTracks();
                int index = 0;
                for (Track track : availableTracks) {
                    final int childIndex = index;
                    final String trackMap = track.getTrackMap();
                    final Texture texture = new Texture(track.getTrackTexture());
                    tracks.put(index, trackMap);

                    Table trackTable = new Table(game.getSkin());
                    trackTable.padLeft(15f);
                    trackTable.padRight(15f);
//                    trackTable.setDebug(true);
                    final TextureAtlas uiAtlas = new TextureAtlas("uiskin.atlas");
                    final Sprite star = new Sprite(uiAtlas.findRegion("star_blue"));
                    final Sprite flag = new Sprite(uiAtlas.findRegion("flag"));
                    Table starsAndFlags = new Table(game.getSkin());
                    Table stars = new Table(game.getSkin());
                    Table flags = new Table(game.getSkin());

                    for (int i = 0; i < track.getDifficulty(); i++) {
                        stars.add(new Image(star));
                    }
                    for (int i = 0; i < track.getLaps(); i++) {
                        flags.add(new Image(flag));
                    }
                    starsAndFlags.add(stars).expandX().left();
                    starsAndFlags.add(flags).expandX().right();
                    trackTable.add(starsAndFlags).padLeft(10f).padRight(10f).expandX().fillX();
                    trackTable.row();
                    ImageButton trackButton = new ImageButton(new TextureRegionDrawable(texture));
                    trackTable.addListener(new ChangeListener() {
                        @Override
                        public void changed(ChangeEvent event, Actor actor) {
                            Table oldSelected = (Table) thumbsHGroup.getChild(selectedTrack);
                            oldSelected.background((Drawable) null);
                            Table newSelected = (Table) thumbsHGroup.getChild(childIndex);
                            newSelected.background(new NinePatchDrawable(backgroundSelected));
                            selectedTrack = childIndex;
                            socket.send(new ChangeTrack(trackMap));
                        }
                    });
                    trackTable.add(trackButton).height(Value.percentHeight(0.55f, thumbsHGroup));
                    trackTable.row();
                    trackTable.add(new TextButton(track.getName(), game.getSkin(), "plain-text")).expand().fill();
                    thumbsHGroup.add(trackTable);
                    index++;
                }
                Table initial = (Table) thumbsHGroup.getChild(0);
                initial.background(new NinePatchDrawable(backgroundSelected));
                updateRaceParameters(msg.getRaceParameters());
            }
        });
    }

    @Override
    public void onPlayerLeftLobby(PlayerLeftLobby msg) {
        lobby.removePlayer(msg.getPlayerId());
    }

    @Override
    public void onPlayerJoined(PlayerJoined msg) {
        lobby.addPlayer(msg.getPlayerId(), msg.getPlayerName(), msg.getKartColor());
    }

    @Override
    public void onLoginError(final LoginError msg) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                final MenuScreen menu = new MenuScreen(game, music, msg);
                music = null;
                switch (msg.getCode()) {
                    case LOBBY_NOT_EXIST:
                        game.replaceScreen(menu);
                        break;
                    case LOBBY_NOT_AVAILABLE:
                        game.replaceScreen(menu);
                        break;
                    case NOT_ERROR:
                        break;
                }
            }
        });
    }

    @Override
    public void onRaceParametersChanged(final RaceParametersChanged msg) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                updateRaceParameters(msg.getParameters());
            }
        });
    }

    private void updateRaceParameters(final Race.Parameters parameters) {
        Table oldSelected = (Table) thumbsHGroup.getChild(selectedTrack);
        oldSelected.background((Drawable) null);

        int newIndex = 0;
        for (Map.Entry track: tracks.entrySet()) {
            if (track.getValue().equals(parameters.track)) {
                newIndex = (Integer) track.getKey();
            }
        }
        Table newSelected = (Table) thumbsHGroup.getChild(newIndex);
        newSelected.background(new NinePatchDrawable(backgroundSelected));

        float totalWidth = 0f;
        for (int i = 0; i < newIndex; i++) {
            totalWidth += thumbsHGroup.getChild(i).getWidth();
        }
        totalWidth = totalWidth - (thumbsScroll.getWidth() / 2) + (thumbsHGroup.getChild(selectedTrack).getWidth() / 2);

        thumbsScroll.setScrollX(totalWidth);

        selectedTrack = newIndex;
    }

    public ClientScreen getClientScreen(
            final Race.Parameters parameters
            , Iterable<? extends KartState> kartsState
            , Map<String, String> players
            , Map<String, KartColor> playersColor
    ) {
        return new ClientScreen(game, parameters, kartsState, players, playersColor, socket, player);
    }
}
