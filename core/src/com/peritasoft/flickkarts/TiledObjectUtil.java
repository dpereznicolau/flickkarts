package com.peritasoft.flickkarts;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import org.poly2tri.Poly2Tri;
import org.poly2tri.geometry.polygon.Polygon;
import org.poly2tri.geometry.polygon.PolygonPoint;
import org.poly2tri.triangulation.delaunay.DelaunayTriangle;

import java.util.List;

public class TiledObjectUtil {
    public static void parseTiledObjectLayer(World world, MapObjects objects) {
        for (MapObject object : objects) {
            Shape shape;
            if (object instanceof PolylineMapObject) {
                shape = createPolyline((PolylineMapObject) object);
            } else {
                continue;
            }
            Body body;
            BodyDef def = new BodyDef();
            def.type = BodyDef.BodyType.StaticBody;
            body = world.createBody(def);

            final FixtureDef fixture = new FixtureDef();
            fixture.shape = shape;

            body.createFixture(fixture);
            shape.dispose();
        }
    }

    private static ChainShape createPolyline(PolylineMapObject polyline) {
        float[] vertices = polyline.getPolyline().getTransformedVertices();
        Vector2[] worldVertices = new Vector2[vertices.length / 2];

        for (int i = 0; i < worldVertices.length; i++) {
            worldVertices[i] = new Vector2(vertices[i * 2] * FlickKarts.UNIT_SCALE, vertices[i * 2 + 1] * FlickKarts.UNIT_SCALE);
            if (i > 0 && worldVertices[i].dst2(worldVertices[i - 1]) <= 0.005f * 0.005f) {
                final Integer polylineId = polyline.getProperties().get("id", Integer.class);
                throw new RuntimeException("vertices " + (i - 1) + " and " + i + " of polyline id " + polylineId + " are too close");
            }
        }

        ChainShape chain = new ChainShape();
        chain.createChain(worldVertices);
        return chain;
    }

    public static Itinerary createItinerary(MapObjects objects) {
        final ItineraryBuilder builder = new ItineraryBuilder();
        for (MapObject object : objects) {
            if (object instanceof PolylineMapObject) {
                PolylineMapObject polyline = (PolylineMapObject) object;
                float[] vertices = polyline.getPolyline().getTransformedVertices();
                Vector2[] itinerary = new Vector2[vertices.length / 2];

                for (int i = 0; i < itinerary.length; i++) {
                    itinerary[i] = new Vector2(vertices[i * 2] * FlickKarts.UNIT_SCALE, vertices[i * 2 + 1] * FlickKarts.UNIT_SCALE);
                }
                builder.setPoints(itinerary);
            } else if (object instanceof RectangleMapObject) {
                RectangleMapObject rectangleMap = (RectangleMapObject) object;
                final int number = Integer.parseInt(rectangleMap.getName());
                final Rectangle rectangle = rectangleMap.getRectangle();
                scaleRectangle(rectangle, FlickKarts.UNIT_SCALE);
                builder.addCheckpoint(new Itinerary.Checkpoint(number, rectangle));
            }
        }
        return builder.build();
    }

    public static Rectangle getRectangle(MapObjects objects, String rectName) {
        MapObject object = objects.get(rectName);
        if (object == null) throw new IllegalArgumentException();
        MapProperties objectProperties = object.getProperties();
        return new Rectangle(objectProperties.get("x", Float.class),
                objectProperties.get("y", Float.class),
                objectProperties.get("width", Float.class),
                objectProperties.get("height", Float.class));
    }

    public static void scaleRectangle(Rectangle rectangle, float unitScale) {
        rectangle.x = rectangle.x * unitScale;
        rectangle.y = rectangle.y * unitScale;
        rectangle.width = rectangle.width * unitScale;
        rectangle.height = rectangle.height * unitScale;
    }

    public static void parseFrictionsLayer(World world, MapObjects objects) {
        Body body;
        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.StaticBody;
        body = world.createBody(def);

        for (MapObject object : objects) {
            if (object instanceof PolygonMapObject) {
                createPolygon((PolygonMapObject) object, body);
            } else {
                continue;
            }
            final Float fr = object.getProperties().get("friction", Float.class);
            body.setUserData(new Terrain(fr));
        }
    }

    private static void createPolygon(PolygonMapObject object, final Body body) {
        float[] vertices = object.getPolygon().getTransformedVertices();
        PolygonPoint[] polygonPoints = new PolygonPoint[vertices.length / 2];

        for (int i = 0; i < polygonPoints.length; i++) {
            polygonPoints[i] = new PolygonPoint(vertices[i * 2] * FlickKarts.UNIT_SCALE, vertices[i * 2 + 1] * FlickKarts.UNIT_SCALE);
            /*if (i > 0 && polygonPoints[i].dst2(polygonPoints[i - 1]) <= 0.005f * 0.005f) {
                final Integer polygonId = object.getProperties().get("id", Integer.class);
                throw new RuntimeException("vertices " + (i - 1) + " and " + i + " of polygon id " + polygonId + " are too close");
            }*/
        }
        final Polygon polygon = new Polygon(polygonPoints);
        Poly2Tri.triangulate(polygon);
        final List<DelaunayTriangle> triangles = polygon.getTriangles();
        for (final DelaunayTriangle triangle : triangles) {
            final int nPoints = triangle.points.length;
            if (nPoints < 3 || nPoints > 8) continue;
            Vector2[] shapePoints = new Vector2[nPoints];
            for (int p = 0; p < shapePoints.length; p++) {
                shapePoints[p] = new Vector2(triangle.points[p].getXf(), triangle.points[p].getYf());
            }
            PolygonShape poly = new PolygonShape();
            poly.set(shapePoints);
            final FixtureDef fixture = new FixtureDef();
            fixture.shape = poly;
            fixture.isSensor = true;
            body.createFixture(fixture);
            poly.dispose();
        }
    }
}
