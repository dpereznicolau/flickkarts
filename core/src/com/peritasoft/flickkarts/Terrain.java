package com.peritasoft.flickkarts;

public class Terrain {
    private final float friction;

    Terrain(float friction) {
        this.friction = friction;
    }

    public float getFriction() {
        return friction;
    }
}
