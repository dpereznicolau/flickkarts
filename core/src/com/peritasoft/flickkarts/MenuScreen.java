package com.peritasoft.flickkarts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.github.czyzby.websocket.data.WebSocketException;
import com.peritasoft.flickkarts.messages.LoginError;

public class MenuScreen extends FlickKartsScreen {
    private final Texture background;
    private Window configWindow;
    private Window joinGameWindow;
    private Window msgWindow;
    private Label msgLabel;
    private TextField playerName;
    private Music music;

    public MenuScreen(final FlickKarts game, Music music) {
        this(game, music, new LoginError());
    }

    public MenuScreen(final FlickKarts game, Music music, LoginError msg) {
        super(game);
        background = new Texture("background.png");
        setupLayout(game.getSkin());
        if (music == null) {
            this.music = Gdx.audio.newMusic(Gdx.files.internal("music/menu.mp3"));
            this.music.setLooping(true);
            this.music.setVolume(0.20f);
            if (!game.isMuted()) {
                this.music.play();
            }
        } else {
            this.music = music;
        }

        switch (msg.getCode()) {
            case LOBBY_NOT_EXIST:
                msgLabel.setText("The race code introduced is invalid");
                msgWindow.setVisible(true);
                break;
            case LOBBY_NOT_AVAILABLE:
                msgLabel.setText("The race you are trying to enter is full");
                msgWindow.setVisible(true);
                break;
            case SERVER_NOT_AVAILABLE:
                msgLabel.setText("Game servers are unavailable");
                msgWindow.setVisible(true);
                break;
            case NOT_ERROR:
            default:
                break;
        }
    }

    private void setupLayout(Skin skin) {
        configWindow = getConfigWindow(skin);
        configWindow.setVisible(false);
        joinGameWindow = getJoinGameWindow(skin);
        joinGameWindow.setVisible(false);
        msgWindow = getMsgWindow(skin);
        msgWindow.setVisible(false);

        final Image backgroundImage = new Image(new TextureRegionDrawable(background), Scaling.fill);
        final Container<Image> background = new Container<Image>(backgroundImage);
        background.setFillParent(true);
        background.fill();
        addActor(background);

        final Table table = new Table();
        table.setFillParent(true);
        addActor(table);

        playerName = new TextField(game.getPlayerName(), skin, "blue");
        playerName.setMaxLength(17);
        playerName.setDisabled(true);
        playerName.setAlignment(Align.center);
        table.add(playerName).width(300).padRight(10).padTop(10).align(Align.right);

        final ImageButton playerButton = new ImageButton(skin, "settings-round");
        table.add(playerButton).padRight(10).padTop(10);
        playerButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                configWindow.setVisible(true);
            }
        });

        table.row();

        final Image titleImage = new Image(new TextureRegionDrawable(new Texture("title.png")));
        table.add(titleImage).padTop(15).padBottom(100).colspan(2);
        table.row();

        final Table buttonTable = new Table();
        buttonTable.defaults().space(50);
        table.add(buttonTable).expand().top();

        final TextButton hostGame = new TextButton("NEW", skin);
        hostGame.padLeft(150).padRight(150);
        buttonTable.add(hostGame).colspan(2);
        buttonTable.row();
        hostGame.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setPlayerName(playerName.getText());
                final ClientLobbyScreen lobby = new ClientLobbyScreen(game, music);
                music = null;
                game.replaceScreen(lobby);
            }
        });

        final TextButton joinGame = new TextButton("JOIN", skin);
        joinGame.padLeft(150).padRight(150);
        buttonTable.add(joinGame).colspan(2);
        buttonTable.row();
        joinGame.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                joinGameWindow.setVisible(true);
            }
        });

        addActor(joinGameWindow);
        addActor(configWindow);
        addActor(msgWindow);
    }

    private Window getConfigWindow(Skin skin) {
        final Window window = new Window("", skin);
        window.setSize(450, 600);
        window.setPosition(getWidth() / 2 - window.getWidth() / 2, getHeight() / 2 - window.getHeight() / 2);
        window.setMovable(true);

        final Label windowTitle = new Label("Configuration", skin, "title");
        window.getTitleTable().add(windowTitle).padRight(20).padTop(-20);

        final Label playerNameLabel = new Label("Player Name:", skin);
        window.add(playerNameLabel).expandX().fillX().padBottom(20);
        window.row();

        final TextField newPlayerName = new TextField(game.getPlayerName(), skin);
        newPlayerName.setMaxLength(17);
        newPlayerName.setAlignment(Align.center);
        setKeyboardFocus(newPlayerName);

        window.add(newPlayerName).expandX().fillX().padBottom(50);
        window.row();

        final ChangeListener closeWindowListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                window.setVisible(false);
            }
        };

        final ChangeListener changePlayerNameListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                String newName = newPlayerName.getText();
                if (!newPlayerName.equals("")) {
                    game.setPlayerName(newName);
                    playerName.setText(newName);
                    closeWindowListener.changed(null, null);
                }
            }
        };

        final TextButton changePlayerName = new TextButton("Set New Name", skin);
        window.add(changePlayerName).expandX().fillX().padBottom(50);
        window.row();
        changePlayerName.addListener(changePlayerNameListener);

        newPlayerName.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ENTER) {
                    changePlayerNameListener.changed(null, null);
                    return true;
                }
                return super.keyUp(event, keycode);
            }
        });


        final TextButton muteUnmute = new TextButton("Mute/Unmute sound", skin);
        muteUnmute.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.muteUnmuteSound();
                if (game.isMuted()) {
                    music.stop();
                } else {
                    music.play();
                }
            }
        });
        window.add(muteUnmute).expandX().fillX().padBottom(50);
        window.row();

        ImageButton close = new ImageButton(skin, "close");
        close.addListener(closeWindowListener);

        window.getTitleTable().add(close).size(40, 40).padRight(-40).padTop(-20);
        window.setClip(false);
        window.setTransform(true);

        return window;
    }

    private Window getJoinGameWindow(Skin skin) {
        final Window window = new Window("", skin);
        window.setSize(450, 400);
        window.setPosition(getWidth() / 2 - window.getWidth() / 2, getHeight() / 2 - window.getHeight() / 2);
        window.setMovable(true);

        final Label windowTitle = new Label("Join Game", skin, "title");
        window.getTitleTable().add(windowTitle).padRight(50).padTop(-20);

        final Label raceCodeLabel = new Label("Race Code:", skin);
        window.add(raceCodeLabel).expandX().fillX().padBottom(20);
        window.row();

        final TextField raceCode = new TextField("", skin);
        raceCode.setMaxLength(6);
        raceCode.setAlignment(Align.center);
        setKeyboardFocus(raceCode);

        window.add(raceCode).expandX().fillX().padBottom(50);
        window.row();

        final ChangeListener joinListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                String code = raceCode.getText();
                if (!code.equals("")) {
                    game.setPlayerName(playerName.getText());
                    final ClientLobbyScreen lobby = new ClientLobbyScreen(game, code, music);
                    music = null;
                    game.replaceScreen(lobby);
                }
            }
        };

        raceCode.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ENTER) {
                    joinListener.changed(null, null);
                    return true;
                }
                return super.keyUp(event, keycode);
            }
        });

        final TextButton joinGame = new TextButton("JOIN", skin);
        joinGame.setDisabled(true);
        window.add(joinGame).expandX().fillX();
        window.row();
        joinGame.addListener(joinListener);

        raceCode.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                String code = raceCode.getText();
                joinGame.setDisabled(code.equals(""));
            }
        });

        ImageButton close = new ImageButton(skin, "close");
        close.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                window.setVisible(false);
            }
        });

        window.getTitleTable().add(close).size(40, 40).padRight(-40).padTop(-20);
        window.setClip(false);
        window.setTransform(true);

        return window;
    }

    private Window getMsgWindow(Skin skin) {
        final Window window = new Window("", skin);
        window.setSize(750, 200);
        window.setPosition(getWidth() / 2 - window.getWidth() / 2, getHeight() / 2 - window.getHeight() / 2);
        window.setMovable(true);

        final Label windowTitle = new Label("Info message", skin, "title");
        window.getTitleTable().add(windowTitle).padRight(175).padTop(-20);

        msgLabel = new Label("", skin);
        window.add(msgLabel).expandX().fillX().padBottom(20);
        window.row();

        ImageButton close = new ImageButton(skin, "close");
        close.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                window.setVisible(false);
            }
        });

        window.getTitleTable().add(close).size(40, 40).padRight(-40).padTop(-20);
        window.setClip(false);
        window.setTransform(true);

        return window;
    }

    @Override
    protected void goBack() {
        super.goBack();
        if (joinGameWindow.isVisible()) {
            joinGameWindow.setVisible(false);
        } else if (configWindow.isVisible()) {
            configWindow.setVisible(false);
        } else if (msgWindow.isVisible()) {
            msgWindow.setVisible(false);
        } else {
            Gdx.app.exit();
        }
    }

    @Override
    public void hide() {
        super.hide();
        if (music != null) {
            music.dispose();
        }
    }

    @Override
    public void dispose() {
        background.dispose();
        super.dispose();
    }
}
