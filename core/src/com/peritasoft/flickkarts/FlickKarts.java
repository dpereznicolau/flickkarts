package com.peritasoft.flickkarts;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class FlickKarts extends Game {
    public static final float UNIT_SCALE = 0.001875f / 4f;

    private static final String PLAYER_NAME = "player-name";
    private static final String MUTED = "muted";

    private final ScreenBuilder screenBuilder;
    private SpriteBatch batch;
    private Skin skin;
    private boolean loadAssets;
    private String playerName;
    private String raceCode;
    private Preferences preferences;
    private boolean muted;

    public FlickKarts(final ScreenBuilder screenBuilder) {
        this.screenBuilder = screenBuilder;
        loadAssets = true;
    }

    public FlickKarts() {
        this(new DefaultScreenBuilder());
    }

    @Override
    public void create() {
        if (loadAssets) {
            batch = new SpriteBatch();
            skin = new Skin(Gdx.files.internal("uiskin.json"));
        }
        preferences = Gdx.app.getPreferences("flickkarts");
        if (playerName == null) {
            playerName = preferences.getString(PLAYER_NAME, System.getProperty("user.name"));
        }
        muted = preferences.getBoolean(MUTED);
        Gdx.input.setCatchKey(Input.Keys.BACK, true);
        setScreen(screenBuilder.build(this));
    }

    @Override
    public void dispose() {
        super.dispose();
        screen.dispose();
        if (loadAssets) {
            skin.dispose();
            batch.dispose();
        }
    }

    public Skin getSkin() {
        return skin;
    }

    public String getRaceCode() {
        return raceCode;
    }

    public void setRaceCode(String raceCode) {
        this.raceCode = raceCode;
    }

    public void replaceScreen(Screen replacement) {
        if (screen != null) {
            final Screen replaced = screen;
            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    replaced.dispose();
                }
            });
        }
        setScreen(replacement);
    }

    public void setLoadAssets(boolean loadAssets) {
        this.loadAssets = loadAssets;
    }

    public Batch getBatch() {
        return batch;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
        if (preferences != null) {
            preferences
                    .putString(PLAYER_NAME, playerName)
                    .flush();
        }
    }

    public boolean isMuted() {
        return muted;
    }

    public void muteUnmuteSound() {
        setMuted(!muted);
    }

    private void setMuted(boolean muted) {
        this.muted = muted;
        if (preferences != null) {
            preferences
                    .putBoolean(MUTED, muted)
                    .flush();
        }
    }

    public interface ScreenBuilder {
        Screen build(FlickKarts game);
    }

    public static class DefaultScreenBuilder implements ScreenBuilder {
        @Override
        public Screen build(FlickKarts game) {
            return new MenuScreen(game, null);
        }
    }
}
