package com.peritasoft.flickkarts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MusicPlayer {
    private final FlickKarts game;
    private final List<Music> musics;
    private int current;
    private Music currentTrack;

    public MusicPlayer(FlickKarts game) {
        this.game = game;
        this.musics = new ArrayList<>();
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t1_adrenaline.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t2_allstarsshow.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t3_bulletproof.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t4_darkcity.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t5_drive.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t6_fuel.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t7_overdrive.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t8_partyrock.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t9_pulseoffreedom.mp3")));
        musics.add(Gdx.audio.newMusic(Gdx.files.internal("music/t10_takeontheworld.mp3")));
        for (Music m : musics) {
            m.setVolume(0.05f);
            m.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music music) {
                    playNextTrack();
                }
            });
        }
        current = 0;
    }

    public void randomize() {
        Collections.shuffle(musics);
        current = 0;
    }

    public void playNextTrack() {
        if (!game.isMuted()) {
            if (currentTrack != null) {
                currentTrack.stop();
            }
            currentTrack = musics.get(current);
            currentTrack.play();
            current++;
            if (current == musics.size()) {
                current = 0;
            }
        }
    }

    public void pauseResumeMusic() {
        if (!game.isMuted()) {
            if (currentTrack != null) {
                if (currentTrack.isPlaying()) {
                    currentTrack.pause();
                } else {
                    currentTrack.play();
                }
            } else {
                playNextTrack();
            }
        }
    }

    public void muteUnmute() {
        game.muteUnmuteSound();
        if (game.isMuted()) {
            stopMusic();
        } else {
            pauseResumeMusic();
        }
    }

    public void stopMusic() {
        if (currentTrack != null && currentTrack.isPlaying()) {
            currentTrack.stop();
        }
    }

    public void dispose() {
        for (Music m : musics) {
            m.dispose();
        }
    }
}
