package com.peritasoft.flickkarts;

import com.github.czyzby.websocket.AbstractWebSocketListener;
import com.github.czyzby.websocket.WebSocket;
import com.github.czyzby.websocket.data.WebSocketException;
import com.peritasoft.flickkarts.messages.*;

public class Client extends AbstractWebSocketListener {
    private final Listener listener;
    private final LobbyListener lobbyListener;

    public Client(Listener listener) {
        this.listener = listener;
        lobbyListener = new NullLobbyListener();
    }

    public Client(LobbyListener lobbyListener) {
        this.lobbyListener = lobbyListener;
        this.listener = new NullListener();
    }

    @Override
    protected boolean onMessage(WebSocket webSocket, Object packet) throws WebSocketException {
        if (packet instanceof GameStarted) {
            lobbyListener.onGameStarted((GameStarted) packet);
        } else if (packet instanceof LoggedIn) {
            lobbyListener.onLoggedIn((LoggedIn) packet);
        } else if (packet instanceof PlayerLeftLobby) {
            lobbyListener.onPlayerLeftLobby((PlayerLeftLobby) packet);
        } else if (packet instanceof Flick) {
            listener.onFlick((Flick) packet);
        } else if (packet instanceof NextTurn) {
            listener.onNextTurn((NextTurn) packet);
        } else if (packet instanceof PlayerLeftRace) {
            listener.onPlayerLeftRace((PlayerLeftRace) packet);
        } else if (packet instanceof RaceFinished) {
            listener.onRaceFinished((RaceFinished) packet);
        } else if (packet instanceof KartClassified) {
            listener.onKartClassified((KartClassified) packet);
        } else if (packet instanceof PickItem) {
            listener.onPickItem((PickItem) packet);
        } else if (packet instanceof PlayerJoined) {
            lobbyListener.onPlayerJoined((PlayerJoined) packet);
        } else if (packet instanceof LoginError) {
            lobbyListener.onLoginError((LoginError) packet);
        } else if (packet instanceof RaceParametersChanged) {
            lobbyListener.onRaceParametersChanged((RaceParametersChanged) packet);
        }
        return FULLY_HANDLED;
    }

    @Override
    public boolean onOpen(WebSocket webSocket) {
        PlayerLogin msg = lobbyListener.onConnect();
        webSocket.send(msg);
        return FULLY_HANDLED;
    }

    private static class NullListener implements Listener {

        @Override
        public void onFlick(Flick msg) {

        }

        @Override
        public void onNextTurn(NextTurn msg) {

        }

        @Override
        public void onPlayerLeftRace(PlayerLeftRace msg) {

        }

        @Override
        public void onRaceFinished(RaceFinished msg) {

        }

        @Override
        public void onKartClassified(KartClassified msg) {

        }

        @Override
        public void onPickItem(PickItem msg) {

        }
    }

    private static class NullLobbyListener implements LobbyListener {

        @Override
        public PlayerLogin onConnect() {
            return null;
        }

        @Override
        public void onGameStarted(GameStarted msg) {

        }

        @Override
        public void onLoggedIn(LoggedIn msg) {

        }

        @Override
        public void onPlayerLeftLobby(PlayerLeftLobby msg) {

        }

        @Override
        public void onPlayerJoined(PlayerJoined msg) {

        }

        @Override
        public void onLoginError(LoginError msg) {

        }

        @Override
        public void onRaceParametersChanged(RaceParametersChanged msg) {

        }
    }

    public interface Listener {
        void onFlick(Flick msg);

        void onNextTurn(NextTurn msg);

        void onPlayerLeftRace(PlayerLeftRace msg);

        void onRaceFinished(RaceFinished msg);

        void onKartClassified(KartClassified msg);

        void onPickItem(PickItem msg);
    }

    public interface LobbyListener {
        PlayerLogin onConnect();

        void onGameStarted(GameStarted msg);

        void onLoggedIn(LoggedIn msg);

        void onPlayerLeftLobby(PlayerLeftLobby msg);

        void onPlayerJoined(PlayerJoined msg);

        void onLoginError(LoginError msg);

        void onRaceParametersChanged(RaceParametersChanged msg);
    }

}
